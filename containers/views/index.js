import * as home from './Landing'
import * as auth from './Auth'
import * as account from './Account'
import * as listing from './Listing'
import * as listings from './Listings'
import * as interestForm from './InterestForm'
import * as favorites from './Favorites'

export default {home, auth, account, listing, listings, interestForm, favorites}
