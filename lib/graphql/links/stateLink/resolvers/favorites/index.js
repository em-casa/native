import * as Mutation from './mutations'

export default {
  defaults: {
    favoritedListings: []
  },
  resolvers: {Mutation}
}
