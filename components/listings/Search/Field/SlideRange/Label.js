import Text from '@/components/shared/Text'
import styles from './styles'

export default (props) => <Text style={styles.labelText} {...props} />
